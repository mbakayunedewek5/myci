<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model 
{
    function getData($id=null)
    {
        if (isset($id)) {
            $this->db->where('user.id', $id);
        }
        
        $this->db->select('user.*, nama_hak_akses');
        $this->db->join('hak_akses', 'hak_akses.id = user.id_hak_akses');
        return $this->db->get('user');
    }
    
    function inputData($data)
    {
        return $this->db->insert('user', $data);
    }
    
    function editData($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('user', $data);
    }
    
    function hapusData($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('user');
    }
}