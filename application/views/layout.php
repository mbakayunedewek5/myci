<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin MyCi</title>
    <style>
        body {
            margin: 0 20%;
        }
        header {
            text-align: center;
        }
        header > h2 {
            padding: 5px;
        }
        footer {
            padding: 5px;
        }
        .wrapper {
            background-color: #e5e5e5;
        }
        .menu {
            text-align: center;
            padding: 5px;
        }
        .menu > a {
            margin: 0 10px;
        }
        .menu > a:hover {
            background-color: #fafafa;
        }
        .content {
            min-height: 320px;
            padding: 30px;
            background-color: #fafafa;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <header>
            <h2>This is Header</h2>
        </header>
        <div class="menu">
            <a href="#">Menu 1</a>
            <a href="#">Menu 2</a>
            <a href="#">Menu 3</a>
            <a href="#">Menu 4</a>
        </div>
        <section>
            <div class="content">
                <?php echo $content; ?>
            </div>
        </section>
        <footer>
            <p>Copyright © This is footer</p>
        </footer>
    </div>
</body>
</html>