<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <script src="main.js"></script>
</head>
<body>
    <h3>Form User</h3>
    <form action="<?php echo isset($user) ? base_url('admin/simpan/').$user->id : base_url('admin/simpan'); ?>" method="post">
        <div>
            <label for="nama">Nama :</label><br>
            <input type="text" name="nama" id="nama" value="<?php echo isset($user) ? $user->nama : ''; ?>">
        </div>
        <br>
        <div>
            <label for="username">Username :</label><br>
            <input type="text" name="username" id="username" value="<?php echo isset($user) ? $user->username : ''; ?>">
        </div>
        <br>
        
        <div>
            <label for="password">Password : </label><br>
            <input type="password" name="password" id="password" value="<?php echo isset($user) ? $user->password : ''; ?>">
        </div>
        <br>

        <button type="submit">Simpan</button>
    </form>
</body>
</html>