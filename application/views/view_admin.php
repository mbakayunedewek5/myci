<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
</head>
<body>
    <h1>Admin</h1>
    <p>hai, ini view admin</p>
    <p><a href="<?php echo base_url('admin/form_tambah'); ?>">Tambah Data</a></p>
    
    <p>Test Helper : <?php echo duit('123450000000'); ?></p>

    <?php if (!empty($users)) : ?>
        <table>
            <thead>
                <tr>
                    <th width="20px">No</th>
                    <th align="left" width="120px">Nama</th>
                    <th align="left" width="100px">Username</th>
                    <th align="left" width="120px">Hak Akses</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $key => $val) : ?>
                    <tr>
                        <td><?php echo $key+1; ?>.</td>
                        <td><?php echo $val->nama; ?></td>
                        <td><?php echo $val->username; ?></td>
                        <td><?php echo $val->nama_hak_akses; ?></td>
                        <td>
                            <a href="<?php echo base_url('admin/form_edit/').$val->id; ?>">Edit</a>
                            &nbsp;|&nbsp;
                            <a href="<?php echo base_url('admin/hapus/').$val->id; ?>">Hapus</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</body>
</html>