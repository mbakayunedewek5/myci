<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mytemplate
{
    protected $ci;

	function __construct()
	{
		$this->ci=&get_instance();
    }
    
    function display($template, $data=null)
    {
        $data['content_lib'] = $this->ci->load->view($template, $data, true);

		$this->ci->load->view('lib_layout', $data);
    }
}