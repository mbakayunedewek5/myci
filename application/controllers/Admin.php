<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel', 'usermodel');
	}
	
	public function index()
	{
		$users = $this->usermodel->getData()->result();

		$data = array('users'=>$users);

		$this->load->view('view_admin', $data);
	}
	
	public function form_tambah()
	{
		$this->load->view('view_form');
	}
	
	public function form_edit($id)
	{
		$user = $this->usermodel->getData($id)->row();

		$data = array('user'=>$user);

		$this->load->view('view_form', $data);
	}
	
	public function simpan($id=null)
	{
		$post = $this->input->post();
		
		if (isset($id)) {
			$in = $this->usermodel->editData($id, $post);
		} else {
			$in = $this->usermodel->inputData($post);
		}

		if ($in) 
			redirect(base_url('admin'));
	}
	
	public function hapus($id)
	{
		$del = $this->usermodel->hapusData($id);
		
		if ($del) 
			redirect(base_url('admin'));
	}

	public function mytemplate()
	{
		$this->load->view('index_template');
	}

	public function dashboard()
	{
		$this->load->view('view_konten');
	}

	public function konten1()
	{
		$this->load->view('konten1');
	}

	public function konten2()
	{
		$this->load->view('konten2');
	}

	public function newkonten1()
	{
		$content = $this->load->view('new_konten1', null, true);

		$this->load->view('layout', array('content'=>$content));
	}

	public function newkonten2()
	{
		$content = $this->load->view('new_konten2', null, true);

		$this->load->view('layout', array('content'=>$content));
	}

	public function libkonten1()
	{
		$this->load->library('mytemplate');

		$this->mytemplate->display('lib_konten1');
	}

	public function libkonten2()
	{
		$this->load->library('mytemplate');

		$this->mytemplate->display('lib_konten2');
	}

	public function user($nama)
	{
		echo 'Hai user admin = '.$nama;
	}
}